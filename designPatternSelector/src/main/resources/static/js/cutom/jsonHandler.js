
 function createRequestJson(){

 var designReqObj = {
   appName : getTaskName(),
   selectedIntentList : getSelectedWAIntentList(),
   componentDetailsList : getFinalComponentList(componentObjectList)
 };


return designReqObj;
//var person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"};
//JSON.stringify(person)
 }

 function setValuesToDisplay(text){
    var jsonObj = JSON.parse(text);
    setDesignReasoning(jsonObj);
    setUmlDiagram(jsonObj);

 }

  function createWARequestJson(){
    var probDescription = document.getElementById("problemDescription");
    var WAReqObj = {
        input : {
            text : probDescription.value.trim()
        },
        context : {}
    };
    return WAReqObj;
  }

/*  function getSelectedWAIntentList(WAResponse){
    var intentList = [];
            for(var i =0; WAResponse != null && WAResponse.intents.length; i++){
                var obj = createWAResponseObj(WAResponse.intents[i].intent, WAResponse.intents[i].confidence);
                intentList.push(obj);
            }
    return intentList;
  }*/

  function createWAResponseObj(WAIntent,WAconfidence){
    var WAResponseObj ={
        intent : WAIntent,
        confidence : WAconfidence
    }
    return WAResponseObj;
  }
 function getFinalComponentList(componentList){
  var compList=[];
  for(var i=0; i<componentList.length; i++){
        if(componentList[i]!= null){
            compList.push(componentList[i]);
        }
  }
  return compList;
 }