var csrfToken='';
function apiTesting(){
    var request = new XMLHttpRequest();

    request.open('GET', 'https://ghibliapi.herokuapp.com/films', true);
    request.setRequestHeader("x-csrf-token", "fetch");
    request.onload = function () {

        var data = JSON.parse(this.response);
        if (request.status >= 200 && request.status < 400) {
            data.forEach(movie => {
                console.log(movie.title);
            });
        } else {
            console.log('error');
        }
    }
    request.send();
    if(request.readyState === 4){
        csrfToken = tryout.getResponseHeader('x-csrf-token');}
 }

 function getAllCrit(){
     var request = new XMLHttpRequest();
     request.open('GET', '/patternCrit/getAll', true);
     //request.withCredentials = true;
     request.onload = function () {

         var data = JSON.parse(this.response);
         if (request.status >= 200 && request.status < 400) {
            setPatternCriteriaList(data);
         } else {
             console.log('error');
         }
     }
     request.send();
     /* if(request.readyState === 4){
             csrfToken = tryout.getResponseHeader('x-csrf-token');
             alert(csrfToken);
             }*/
  }

  function submitData(){
  var http = new XMLHttpRequest();
  var url = '/designPattern/suggest';
  var reqBody = JSON.stringify(createRequestJson());
  http.open('POST', url, true);
   //http.withCredentials = true;
  //Send the proper header information along with the request
  http.setRequestHeader('Content-type', 'application/json; charset="utf-8"');

  http.onload = function() {//Call a function when the state changes.
      if(http.readyState == 4 && http.status == 200) {
          //alert(http.responseText);
          setValuesToDisplay(http.responseText);
      }
  }
  http.send(reqBody);

  }

  function getIntents(){
         var request = new XMLHttpRequest();
         request.open('POST', 'https://gateway.watsonplatform.net/assistant/api/v1/workspaces/5763fb4a-f8dd-4f4d-818e-f0d39e12449c/message?version=2018-09-20&nodes_visited_details=false', true, 'apikey','Ov353L_HdQg-Rr4FExbb4fP6PsNaoNJRQavEulHJmKYI');
         //request.withCredentials = true;
         var reqBody = JSON.stringify(createWARequestJson());
         request.setRequestHeader("Authorization", "Basic " + btoa("apikey:Ov353L_HdQg-Rr4FExbb4fP6PsNaoNJRQavEulHJmKYI"));
         request.setRequestHeader('Content-type', 'application/json; charset="utf-8"');
         //request.setRequestHeader("Access-Control-Allow-Origin", "http://localhost:8080");
         //request.setRequestHeader("Access-Control-Allow-Methods", "POST");
         request.onload = function () {

             var data = JSON.parse(this.response);
             if (request.status >= 200 && request.status < 400) {
                //alert(data);
                setIntentList(data);
             } else {
                 console.log('error');
             }
         }
         request.send(reqBody);
         /* if(request.readyState === 4){
                 csrfToken = tryout.getResponseHeader('x-csrf-token');
                 alert(csrfToken);
                 }*/
      }

     /* function getParticipantList(WAResponse){
        var url = '/designPattern/suggest';
          var reqBody = JSON.stringify(getWAIntentList());
          http.open('POST', url, true);
           //http.withCredentials = true;
          //Send the proper header information along with the request
          http.setRequestHeader('Content-type', 'application/json; charset="utf-8"');

          http.onload = function() {//Call a function when the state changes.
              if(http.readyState == 4 && http.status == 200) {
                  alert(http.responseText);
                  setValuesToDisplay(http.responseText);
              }
          }
          http.send(reqBody);
      }*/

      function setIntentList(WAResponse){


          var intentList = document.getElementById("simplifiedIntents-list");
          intentList.innerHTML = "";

          for(var i =0; WAResponse != null && i<WAResponse.intents.length; i++) {

                  intentList.innerHTML +='<div class="col-md-8 pull-right">'
                                              +'<div class="checkbox col-md-12">'
                                                  +'<label for="prependedcheckbox">'+WAResponse.output.text[0]+'</label>'
                                                  +'<input type="checkbox" id="'+WAResponse.intents[i].intent+'" name="'+WAResponse.intents[i].confidence+'" value="1">'

                                              +'</div>'
                                              +'<div class="checkbox col-md-12"> Suggested Pattern :'+WAResponse.intents[i].intent+'&nbsp;&nbsp;&nbsp; Confidence : '+WAResponse.intents[i].confidence
                                              +'</div>'

                                          +'</div>';

          }


      }

      function getSelectedWAIntentList(){
          var intentList =[];
          var selectedCheckbox = document.querySelectorAll('div#simplifiedIntents-list input[type=checkbox]:checked');
          for (var i = 0; i < selectedCheckbox.length; i++) {
              intentList.push(createWAResponseObj(selectedCheckbox[i].name,selectedCheckbox[i].value));
          }
          return intentList;
      }