var componentObjectList = [];
var componentId=0;
var componentIdIndex = [];
var componentNameIndex = [];
var updateModel= false;
var updatedIndex =0;


function createComponentObject(){
   var component = createComponent();

   if(updateModel){
        componentNameIndex[updatedIndex]=component.name;
        componentIdIndex[updatedIndex]=componentId;
    //alert(JSON.stringify(getRelationshipsObjects()));
        //alert(JSON.stringify(component));
        componentId++;
        componentObjectList[updatedIndex]=component;
   }else{
    componentNameIndex.push(component.name);
    componentIdIndex.push(componentId);
    //alert(JSON.stringify(getRelationshipsObjects()));
    //alert(JSON.stringify(component));
    componentId++;
    componentObjectList.push(component);
   // alert(getPatternCritIdList());
   }
    deSelectAllCheckBoxes();
}
function createComponent(){
 var component = {
        name: getComponentName(),
        id : componentId,
        componentInteractionList       : getRelationshipsObjects(),
        selectedPatternCritList : createPatternCritObjcet(getPatternCritObjList())

    };
   return component;
}

function getComponentName(){
    name = $("#textinput").val();
    $("#textinput").val("");
    return name;
}

function getRelationshipsObjects(checkboxValue){
    var selectedCheckbox = document.querySelectorAll('div#pattern-relationships input[type=checkbox]:checked');
    var relationShipObjectList = [];

    for (var i = 0; i < selectedCheckbox.length; i++) {
        if(selectedCheckbox[i].value == "1"){
            var selectedDropdownValues = document.querySelectorAll('#selected-component-is-a>li')
            createRelationshipObject("IS_A",selectedDropdownValues,relationShipObjectList) ;
            clearComponentSelection('#selected-component-is-a');
         //   alert("selectedDropdownvalues   "+selectedDropdownValues);
        }else if(selectedCheckbox[i].value == "2"){
            var selectedDropdownValues = document.querySelectorAll('#selected-component-agg>li')
            createRelationshipObject("HAS_A",selectedDropdownValues,relationShipObjectList);
            clearComponentSelection('#selected-component-agg');
        }else if(selectedCheckbox[i].value == "3"){
            var selectedDropdownValues = document.querySelectorAll('#selected-component-comp>li')
            createRelationshipObject("MUSTHAVE",selectedDropdownValues,relationShipObjectList);
            clearComponentSelection('#selected-component-comp');
        }else if(selectedCheckbox[i].value == "4"){
            var selectedDropdownValues = document.querySelectorAll('#selected-component-asso>li')
            createRelationshipObject("USE",selectedDropdownValues,relationShipObjectList);
            clearComponentSelection('#selected-component-asso');
        }else{
        }
    }
    return relationShipObjectList;
}

function createRelationshipObject(relationName,componentList,relationShipObjectList){
    for (var i = 0; i < componentList.length  ; i++) {
        if( componentList[i]!='undefined'){
            var relationShip = {
                componentRelationType :relationName,
                componentId :getComponentId(componentList[i].childNodes[0].innerText)
            };
            relationShipObjectList.push(relationShip);
        }
    }
}

function getComponentId(componentName){
    var compIndex = componentNameIndex.indexOf(componentName);

    var compId = componentIdIndex[compIndex];
    //var compId = componentObjectList.find(getId).id;
    //function getId(value, index, array) {
    //    return (value.name==componentName) ? value : -1;
    //}
    return compId;
}

function getPatternCritObjList(){
    var patternCritList =[];
    var selectedCheckbox = document.querySelectorAll('div#pattern-crit-list input[type=checkbox]:checked');
    for (var i = 0; i < selectedCheckbox.length; i++) {
        patternCritList.push(selectedCheckbox[i].id);
    }
    return patternCritList;
}

function createPatternCritObjcet(patternCritList){
    var patternCritobjList =[];
    var patternCritObj={ patternCategory: "CREATIONAL",patternCritId : patternCritList};
    patternCritobjList.push(patternCritObj);
    return patternCritobjList;
}

function clearComponentSelection(uiId){
    var lis = document.querySelectorAll(uiId+' li');
    for(var i=0; li=lis[i]; i++) {
        li.parentNode.removeChild(li);
    }
}


function deSelectAllCheckBoxes(){
    var selectedCheckbox = document.querySelectorAll('div#pattern-crit-list input[type=checkbox]:checked');
    for (var i = 0; i < selectedCheckbox.length; i++) {
        selectedCheckbox[i].checked = false;;
    }

    var selectedCheckbox1 = document.querySelectorAll('div#pattern-relationships input[type=checkbox]:checked');
        for (var i = 0; i < selectedCheckbox1.length; i++) {
            selectedCheckbox1[i].checked = false;;
        }

}
/*function getSelectedDropDownValesPerRelation(checkboxValue){


if(checkboxValue == "1"){
var selectedDropdownvalues = document.querySelectorAll('#selected-component-is-a>li')
alert("selectedDropdownvalues   "+selectedDropdownvalues);
}else if(checkboxValue == "2"){
var selectedDropdownvalues = document.querySelectorAll('#selected-component-agg>li')

}else if(checkboxValue == "3"){
var selectedDropdownvalues = document.querySelectorAll('#selected-component-comp>li')

}else{
}

}*/
function getSelectedPatternCritIds(){


}

function getTaskName(){
    return document.getElementById("taskName").value;
}

function removeComponents(compId){

  var nameIndex = componentNameIndex.indexOf(compId);
  delete componentNameList[nameIndex];
  delete componentNameIndex[nameIndex];
  delete componentIdIndex[nameIndex];

  delete componentObjectList[nameIndex];
  var element  = elementList2[nameIndex];
  graph1.removeCells([element]);
  delete elementList2[nameIndex];
  removeElement('main_'+compId);
  updateComponenetDropDown(componentNameList);

}

/*function removeCompRelation(index){
    var comp = componentObjectList[index];
    for(var i=0; comp.componentInteractionList != 'undefined' && i<comp.componentInteractionList.length; i++){
        var idIndex = componentIdIndex[comp.componentInteractionList[i].componentId];
        var affectedComp = componentObjectList[idIndex];
    }
}*/

function updateComponents (compId){
    updatedIndex = componentNameIndex.indexOf(compId);
    updateModel =true;
    var comp = componentObjectList[updatedIndex];
    $("#textinput").val(comp.name);
    loadComponentList(comp);
    loadSelectedCriteria(comp);

}

function loadSelectedCriteria(comp){
    for(var i=0; typeof comp.selectedPatternCritList !== 'undefined' && i<comp.selectedPatternCritList.length; i++){
        for(var j=0; j<comp.selectedPatternCritList[i].patternCritId.length; j++){
            document.getElementById(comp.selectedPatternCritList[i].patternCritId[j]).checked = true;
        }
    }
}

function loadComponentList(comp){
    for(var i=0; typeof  comp.componentInteractionList !== 'undefined' && i<comp.componentInteractionList.length; i++){

        if(comp.componentInteractionList[i].componentRelationType == "IS_A"){
         var idIndex = componentIdIndex.indexOf(comp.componentInteractionList[i].componentId);
         var name = componentNameIndex[idIndex];
         var select = document.getElementById("selected-component-is-a");
         select.innerHTML += '<li id="is_'+name+'_'+buttonCount+'"><a href="#">'+name+'</a>&nbsp;'
                +'<button type="button" class="btn btn-danger btn-xs" onclick="removeElement(\''+'is_'+name+'_'+buttonCount+'\')" > x</button>'
                +'</li>';
                buttonCount++;
                document.getElementById("relations-is-a").checked = true;

        }else if(comp.componentInteractionList[i].componentRelationType == "HAS_A"){
        var idIndex = componentIdIndex.indexOf(comp.componentInteractionList[i].componentId);
                 var name = componentNameIndex[idIndex];
                 var select = document.getElementById("selected-component-agg");
                 select.innerHTML += '<li id="ag_'+name+'_'+buttonCount+'"><a href="#">'+name+'</a>&nbsp;'
                        +'<button type="button" class="btn btn-danger btn-xs" onclick="removeElement(\''+'ag_'+name+'_'+buttonCount+'\')" > x</button>'
                        +'</li>';
                        buttonCount++;
                 document.getElementById("relations-agg").checked = true;

        }else if(comp.componentInteractionList[i].componentRelationType == "MUSTHAVE"){
        var idIndex = componentIdIndex.indexOf(comp.componentInteractionList[i].componentId);
                 var name = componentNameIndex[idIndex];
                 var select = document.getElementById("selected-component-comp");
                 select.innerHTML += '<li id="co_'+name+'_'+buttonCount+'"><a href="#">'+name+'</a>&nbsp;'
                        +'<button type="button" class="btn btn-danger btn-xs" onclick="removeElement(\''+'co_'+name+'_'+buttonCount+'\')" > x</button>'
                        +'</li>';
                        buttonCount++;
                 document.getElementById("relations-comp").checked = true;
        }else if(comp.componentInteractionList[i].componentRelationType == "USE"){
        var idIndex = componentIdIndex.indexOf(comp.componentInteractionList[i].componentId);
                 var name = componentNameIndex[idIndex];
                 var select = document.getElementById("selected-component-asso");
                 select.innerHTML += '<li id="as_'+name+'_'+buttonCount+'"><a href="#">'+name+'</a>&nbsp;'
                        +'<button type="button" class="btn btn-danger btn-xs" onclick="removeElement(\''+'as_'+name+'_'+buttonCount+'\')" > x</button>'
                        +'</li>';
                        buttonCount++;
                        document.getElementById("relations-asso").checked = true;
        }else{
        }
    }
}
