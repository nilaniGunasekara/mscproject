function setDesignReasoning(obj){

    var status = obj.status;
    if(status == "SUCCESS"){
        var patternDescription = obj.patternExplanation;
        //var relatedPatterns = obj.designPattern.relatedPatternList;
        var consequencesList = obj.consequencesList;
        var intent = obj.patternIntent;
        var patternName = obj.patternName;
        var patternCategory = obj.patternCategory;
        var applicabilities = obj.applicabilityList;

        var reasoningHtml = '<br>'
            +'<hr>'
            +'<div class="row">'
                    +'<div class="col-md-4"> <h6>Pattern Description</h6></div>'
                    +'<div class="col-md-8"> <ui> ';
                   for(var i=0;i<patternDescription.length;i++){
                           reasoningHtml += '<li><a href="#">'+patternDescription[i]+'</a></li>';
                       }

             reasoningHtml += ' </ui></div>'
                +'</div>'

            +'<div class="row">'
            +'<div class="col-md-4"> <h6>Pattern name </h6></div>'
        +'<div class="col-md-8"> :'+patternName+'</div>'
        +'</div>'
        +'<div class="row">'
            +'<div class="col-md-4"> <h6>Pattern category </h6></div>'
            +'<div class="col-md-8"> : '+patternCategory+'</div>'
        +'</div>'
        +'<div class="row">'
            +'<div class="col-md-4"> <h6>Pattern intent </h6></div>'
            +'<div class="col-md-8">  : '+intent+'</div>'
        +'</div>'

        +'<div class="row">'
            +'<div class="col-md-4"> <h6>Pattern applicabilities </h6></div>'
            +'<div class="col-md-8">'
            +'<ui>';


        for(var i=0;i<applicabilities.length;i++){
            reasoningHtml += '<li><a href="#">'+applicabilities[i].description+'</a></li>';
        }

        reasoningHtml += '</ui>'
        +'</div>'
        +'</div>'
        +'<div class="row">'
            +'<div class="col-md-4"> <h6>Pattern consequences </h6></div>'
        +'<div class="col-md-8">'
            +'<ui>';

        for(var i=0;i<consequencesList.length;i++){
            reasoningHtml += '<li><a href="#">'+consequencesList[i].description+'</a></li>';
        }

        reasoningHtml += '</ui>'
        +'</div>'
        +'</div>'
        +'<br>';
        var reasoning = document.getElementById("pattern-reasoning");
        reasoning.innerHTML += reasoningHtml;
    }else{
        var errorMessage = obj.errorMessage;
        var reasoning = document.getElementById("pattern-reasoning");
        reasoning.innerHTML = '<div>'+errorMessage+'</div>';
    }
}