var buttonCount =0;
function updateComponenetDropDown(elementList){
    var compIsAList = document.getElementById("component-dropdown-is-a");
    var compAggList = document.getElementById("component-dropdown-agg");
    var compComList = document.getElementById("component-dropdown-comp");
    var compAssoList = document.getElementById("component-dropdown-asso");
    compIsAList.innerHTML = "";
    compAggList.innerHTML = "";
    compComList.innerHTML = "";
    compAssoList.innerHTML = "";
    for(var i = 0; i < elementList.length; i++) {
        var opt = elementList[i];
        if(typeof opt !== 'undefined'){
            compIsAList.innerHTML += '<li><a href="#">'+opt+'</a></li>';
            compAggList.innerHTML += '<li><a href="#">'+opt+'</a></li>';
            compComList.innerHTML += '<li><a href="#">'+opt+'</a></li>';
            compAssoList.innerHTML += '<li><a href="#">'+opt+'</a></li>';
        }
    }
}

$("body").on('click', '#component-dropdown-is-a li a', function () {
    var selectedValue = $(this).text();
    var select = document.getElementById("selected-component-is-a");
    select.innerHTML += '<li id="is_'+selectedValue+'_'+buttonCount+'" name="'+selectedValue+'"><a href="#">'+selectedValue+'</a>&nbsp;'
        +'<button type="button" class="btn btn-danger btn-xs" onclick="removeElement(\''+'is_'+selectedValue+'_'+buttonCount+'\')" > x</button>'
        +'</li>';
    buttonCount++;
});

$("body").on('click', '#component-dropdown-asso li a', function () {
    var selectedValue = $(this).text();
    var select = document.getElementById("selected-component-asso");
    select.innerHTML += '<li id="as_'+selectedValue+'_'+buttonCount+'"><a href="#">'+selectedValue+'</a>&nbsp;'
        +'<button type="button" class="btn btn-danger btn-xs" onclick="removeElement(\''+'as_'+selectedValue+'_'+buttonCount+'\')" > x</button>'
        +'</li>';
        buttonCount++;
});

$("body").on('click', '#component-dropdown-agg li a', function () {
    var selectedValue = $(this).text();
    var select = document.getElementById("selected-component-agg");
    select.innerHTML += '<li id="ag_'+selectedValue+'_'+buttonCount+'"><a href="#">'+selectedValue+'</a>&nbsp;'
        +'<button type="button" class="btn btn-danger btn-xs" onclick="removeElement(\''+'ag_'+selectedValue+'_'+buttonCount+'\')" > x</button>'
        +'</li>';
        buttonCount++;
});

$("body").on('click', '#component-dropdown-comp li a', function () {
    var selectedValue = $(this).text();
    var select = document.getElementById("selected-component-comp");
    select.innerHTML += '<li id="co_'+selectedValue+'_'+buttonCount+'"><a href="#">'+selectedValue+'</a>&nbsp;'
        +'<button type="button" class="btn btn-danger btn-xs" onclick="removeElement(\''+'co_'+selectedValue+'_'+buttonCount+'\')" > x</button>'
        +'</li>';
        buttonCount++;
});

/*function setPatternCriteriaList(patternCritList){
    var criteriaCheckBoxes = "";
    var critList = document.getElementById("pattern-crit-list");
    critList.innerHTML = "";

    for(var i = 0; i < patternCritList.length; i++) {
            var critDescription = patternCritList[i].description;
            var critId = patternCritList[i].id;

            critList.innerHTML +='<div class="col-md-12">'
                                        +'<div class="checkbox ">'
                                            +'<label for="prependedcheckbox">'+critDescription+'</label>'
                                            +'<input type="checkbox" id="'+critId+'" name="'+critId+'" value="1">'
                                        +'</div>'
                                    +'</div>';

    }
}*/

function setPatternCriteriaList(patternCritList){
    var criteriaCheckBoxes = "";

    var critList = document.getElementById("pattern-crit-list");
    critList.innerHTML = "";

    for(var i = 0; i < patternCritList.length; i++) {
		var critType = patternCritList[i].critType;
		    var subCritList = document.getElementsByName(critType);
			if(subCritList == null || subCritList.length==0){
				critList.innerHTML += '<div class="col-md-11 well well-sm" name="'+critType+'" data-toggle="collapse" data-target="#'+critType+'">'
				+'<a href="#">'+critType+'</a>'
				+'</div>'
				+'<div id="'+critType+'" class="col-md-12 pull-right collapse"></div>';
			}
			var critDiv = document.getElementById(critType);

            var critDescription = patternCritList[i].description;
            var critId = patternCritList[i].id;

            critDiv.innerHTML +='<div class="col-md-12 ">'
                                        +'<div class="checkbox  ">'
                                            +'<label for="prependedcheckbox">'+critDescription+'</label>'
                                            +'<input type="checkbox" id="'+critId+'" name="'+critId+'" value="1">'
                                        +'</div>'
                                    +'</div>';

    }
}

function test(){
    //alert("This is a test");
}

function enableDropDown(checkboxId){
    var dropdownId = "";
    if(checkboxId == 'relations-is-a'){
        dropdownId = '#button-dropdown-is-a';
    }else if(checkboxId == 'relations-agg'){
        dropdownId = '#button-dropdown-agg';
    }else if(checkboxId == 'relations-comp'){
        dropdownId = '#button-dropdown-comp';
    }else if(checkboxId == 'relations-asso'){
             dropdownId = '#button-dropdown-asso';
    }else{
        return;
    }
    checkboxId='#'+checkboxId;
    $(checkboxId).click(function() {
       if ($(this).is(':checked')) {
            $(dropdownId).prop("disabled", false);
       } else {
            $(dropdownId).prop('disabled', true);
       }
    });
}

function removeElement(id) {
    var element = document.getElementById(id);
    element.parentNode.removeChild(element);
}