package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import java.util.List;

@Data
public class DesignPatternRecomendationResponse {
 private List<String > UmlDiagramList;
 private String  status;
 private String  errorMessage;
 private String  patternName;
 private PatternCategory  patternCategory;
 private String  patternIntent;
 private List<String>  patternExplanation;
 private List<Applicability>  applicabilityList;
 private List<Consequences>  consequencesList;
}
