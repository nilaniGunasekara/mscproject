package uom.cse.msc.designPatternSelector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.PatternCritRank;
import uom.cse.msc.designPatternSelector.repositories.PatternCritRankRepository;

import java.util.List;

@Service
public class PatternCritRankService {
    @Autowired
    private PatternCritRankRepository patternCritRankRepository;

    public PatternCritRank save(final PatternCritRank patternCritRank){
        return patternCritRankRepository.save(patternCritRank);
    }

    public List<PatternCritRank> saveAll(final List <PatternCritRank> patternCritRankList){
        return patternCritRankRepository.saveAll(patternCritRankList);
    }

    public List<PatternCritRank> getPatternCritRankById(final List<Integer> critIds){
        return patternCritRankRepository.findByPattrnCritIdIn(critIds);
        //return patternCritRankRepository.findByIdIn(critIds);
    }
}
