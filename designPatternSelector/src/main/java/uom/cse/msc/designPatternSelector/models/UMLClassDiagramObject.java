package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import java.util.List;

@Data
public class UMLClassDiagramObject {
    private int objId;
    private String objName;
    private Representaion representaion;
    private List<Integer> connectedObjid;
    private List<String>connectedObjName;
    private Collaboration collaboration;
    private List<String> methodList;
    private List<String> attributeList;
}
