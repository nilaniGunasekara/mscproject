package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "applicability")
public class Applicability {
    @Id
    @GeneratedValue
    private int id;
    private String description;
}
