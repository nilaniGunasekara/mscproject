package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import java.util.List;

@Data
public class DesignPatternRecomendationRequest {
    private String appName;
    private List<IntentObject> selectedIntentList;
    private List<ComponentDetails> componentDetailsList;
}
