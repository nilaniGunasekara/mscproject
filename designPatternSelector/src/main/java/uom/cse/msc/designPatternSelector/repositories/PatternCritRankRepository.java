package uom.cse.msc.designPatternSelector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uom.cse.msc.designPatternSelector.models.PatternCritRank;

import java.util.List;

@Repository
public interface PatternCritRankRepository extends JpaRepository<PatternCritRank, Integer> {

    List<PatternCritRank> findByIdIn(List<Integer> critIds);

    List<PatternCritRank> findByPattrnCritIdIn(List<Integer> critIds);
}
