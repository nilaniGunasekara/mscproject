package uom.cse.msc.designPatternSelector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.PatternRelationship;
import uom.cse.msc.designPatternSelector.repositories.PatternRelationshipRepository;

import java.util.List;

@Service
public class PatternRelationshipService {
    @Autowired
    private PatternRelationshipRepository patternRelationshipRepository;

    public PatternRelationship save(final PatternRelationship patternRelationship){
        return patternRelationshipRepository.save(patternRelationship);
    }

    public List<PatternRelationship> saveAll(final List<PatternRelationship> patternRelationshipList){
        return patternRelationshipRepository.saveAll(patternRelationshipList);
    }


    public List<PatternRelationship> getRelatedPatterns() {
        return patternRelationshipRepository.findAll();
    }
}
