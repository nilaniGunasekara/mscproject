package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "pattern_Relationship")
public class PatternRelationship {
    @Id
    @GeneratedValue
    private int id;
    private int patternId;
    private int relatedPatternId;
}
