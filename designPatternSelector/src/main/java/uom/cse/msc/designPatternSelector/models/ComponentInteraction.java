package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

@Data
public class ComponentInteraction {
    private int componentId;
    private ComponentRelationType componentRelationType;
}
