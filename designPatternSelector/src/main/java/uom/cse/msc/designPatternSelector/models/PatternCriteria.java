package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pattern_criteria")
public class PatternCriteria {
    @Id
    @GeneratedValue
    private int id;
    private  String description;
    private PatternCritType critType;

}
