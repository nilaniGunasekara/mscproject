package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "consequence")
public class Consequences {
    @Id
    @GeneratedValue
    private int id;
    private String description;

}
