package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

@Data
public class IntentObject {
    private String intent;
    private double confidence;
}
