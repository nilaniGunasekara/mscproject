package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "participant")
public class Participant {

    @Id
    @GeneratedValue
    private int id;

    private String name;
    private Representaion representaion;
    @ElementCollection
    private List<String> methodList;
    @ElementCollection
    private List<String> attributeList;
    private String description;

}
