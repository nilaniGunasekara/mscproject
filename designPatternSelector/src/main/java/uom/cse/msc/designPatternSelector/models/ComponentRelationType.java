package uom.cse.msc.designPatternSelector.models;

public enum ComponentRelationType {
    HAS_A,
    IS_A,
    MUSTHAVE,
    USE
}
