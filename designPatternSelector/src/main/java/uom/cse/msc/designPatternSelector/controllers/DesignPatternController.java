package uom.cse.msc.designPatternSelector.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uom.cse.msc.designPatternSelector.models.DesignPattern;
import uom.cse.msc.designPatternSelector.models.DesignPatternRecomendationRequest;
import uom.cse.msc.designPatternSelector.models.DesignPatternRecomendationResponse;
import uom.cse.msc.designPatternSelector.models.WAResponse;
import uom.cse.msc.designPatternSelector.services.DesignPatternService;

import java.util.List;

@RestController
@RequestMapping("/designPattern")

public class DesignPatternController {

    @Autowired
    private DesignPatternService designPatternService;

    @RequestMapping(value = "/add" , method = RequestMethod.POST)
    public DesignPattern create(@RequestBody DesignPattern designPattern){
        return designPatternService.create(designPattern);
    }

    @RequestMapping(value = "/suggest", method = RequestMethod.POST)
    public DesignPatternRecomendationResponse suggestDesignPattern(@RequestBody final DesignPatternRecomendationRequest patternRecomendationRequest){
        DesignPatternRecomendationRequest response =patternRecomendationRequest;
        return designPatternService.getRecommendations(patternRecomendationRequest);
       // return null;
    }

    /*@RequestMapping(value = "/simplifiedIntent" , method = RequestMethod.POST)
    public DesignPattern create(@RequestBody List<WAResponse> waResponse){
        return designPatternService.create(designPattern);
    }*/
}
