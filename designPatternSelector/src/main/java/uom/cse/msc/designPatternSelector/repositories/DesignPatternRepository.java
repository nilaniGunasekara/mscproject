package uom.cse.msc.designPatternSelector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import uom.cse.msc.designPatternSelector.models.DesignPattern;

import java.util.Collection;
import java.util.List;

@Repository
public interface DesignPatternRepository extends JpaRepository<DesignPattern, Integer> {
    List<DesignPattern> findByIdIn(List<Integer> integers);
    DesignPattern findByName(String name);
}
