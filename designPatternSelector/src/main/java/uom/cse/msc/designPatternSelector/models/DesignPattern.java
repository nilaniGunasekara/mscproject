package uom.cse.msc.designPatternSelector.models;

import lombok.Data;
import org.hibernate.Criteria;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "design_pattern")
public class DesignPattern {
    @Id
    @GeneratedValue
    private int id;

    private String name;
    @ElementCollection
    private List<String> descriptionList;
    private String intent;
    private PatternCategory patternCategory;

    @OneToMany
    private List<Applicability> applicabilities;

    @OneToOne
    @JoinColumn(name = "structure_id")
    private PatternStructure patternStructure;

    @ManyToMany
    @JoinTable(
            name = "pattern_crit",
            joinColumns = { @JoinColumn(name = "pattern_id") },
            inverseJoinColumns = { @JoinColumn(name = "crit_id") }
    )
    private List<PatternCriteria> patternCriteria;

    @ManyToMany
    @JoinTable(
            name = "pattern_conseq",
            joinColumns = { @JoinColumn(name = "pattern_id") },
            inverseJoinColumns = { @JoinColumn(name = "conseq_id") }
    )
    private List<Consequences> consequencesList;

    @ManyToMany
    @JoinTable(
            name = "pattern_relatedPattern",
            joinColumns = { @JoinColumn(name = "pattern_id") },
            inverseJoinColumns = { @JoinColumn(name = "rp_id") }
    )
    private List<RelatedPattern> relatedPatternList;

}
