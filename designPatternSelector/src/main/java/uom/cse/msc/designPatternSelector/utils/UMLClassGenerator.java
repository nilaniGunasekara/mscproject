package uom.cse.msc.designPatternSelector.utils;

import org.springframework.stereotype.Component;
import uom.cse.msc.designPatternSelector.models.Collaboration;
import uom.cse.msc.designPatternSelector.models.Representaion;
import uom.cse.msc.designPatternSelector.models.UMLClassDiagramObject;
import uom.cse.msc.designPatternSelector.models.UMLClassDiagramObjectStructure;

import java.util.ArrayList;
import java.util.List;

@Component
public class UMLClassGenerator {

    private static int gridHeight = 20000;
    private static int gridWidth =20000;
    private static int positionX = 10;
    private static int positionY =10;
    private static int objCount =0;

    private String initializePanel(final String divId, int gridWidth, int gridHeight){
        setValuesToDefault();
        String graphInit = " var graph2 = new joint.dia.Graph();"
                + "new joint.dia.Paper({"
                + "el: document.getElementById('"+divId+"'),"
                +"width: "+gridWidth+","
                +"height: "+gridHeight+","
                +"gridSize: 1,"
                +"model: graph2,"
                +"background: {"
                    + "color: 'rgba(0, 255, 0, 0.3)'"
                + "}"
                +"});"
        +"var uml = joint.shapes.uml;";
        return graphInit;
    }

    private String createInterface(final String objName, final List<String> methodList,
                                   final List<String> attributeList,int []size, int []posistion){

        String interfaceComponent = objName+": new uml.Interface({"
                +" position: { x:"+posistion[0]+"  , y: "+posistion[1]+" },"
                +"size: { width: "+size[0]+", height: "+size[1]+" },"
                +"name: '"+objName+"',"
                +"attributes: [";
                for(int i =0; attributeList != null && i<attributeList.size();i++){
                    if(i != 0){
                        interfaceComponent += ",";
                    }
                    interfaceComponent += "'"+attributeList.get(i)+"'";
                }

                interfaceComponent += "],"
                +"methods: [";

                for(int i =0; methodList != null && i<methodList.size();i++){
                    if(i != 0){
                        interfaceComponent += ",";
                    }
                interfaceComponent += "'"+methodList.get(i)+"'";
                }
                interfaceComponent += "],"
                +"attrs: {"
                +"'.uml-class-name-rect': {"
                +"fill: '#feb662',"
                        +"stroke: '#ffffff',"
                        +"'stroke-width': 0.5"
                +"},"
                +"'.uml-class-attrs-rect': {"
                +"fill: '#fdc886',"
                        +"stroke: '#fff',"
                        +"'stroke-width': 0.5"
                +"},"
                +"'.uml-class-methods-rect': {"
                +"fill: '#fdc886',"
                        +"stroke: '#fff',"
                        +"'stroke-width': 0.5"
                +"},"
                +"'.uml-class-attrs-text': {"
                +"ref: '.uml-class-attrs-rect',"
                        +"'ref-y': 0.5,"
                        +"'y-alignment': 'middle'"
                +"},"
                +"'.uml-class-methods-text': {"
                +"ref: '.uml-class-methods-rect',"
                        +"'ref-y': 0.5,"
                        +"'y-alignment': 'middle'"
                +"} } })";
        return interfaceComponent;
    }
    private String createAbstractClass(final String objName, List<String> attributeList, final List<String> methodList, int[] size, int[] posistion){

        String abstractClassComponent = objName+": new uml.Abstract({"
                + "position: { x:"+posistion[0]+", y: "+posistion[1]+" },"
                + "size: { width: "+size[0]+", height: "+size[1]+" },"
                +"name: '"+objName+"',"
                +"attributes: [" ;
                for(int i =0; attributeList != null && i<attributeList.size();i++){
                    if(i != 0){
                        abstractClassComponent += ",";
                    }
                    abstractClassComponent += "'"+attributeList.get(i)+"'";
                }
                 abstractClassComponent += "],"
                +"methods: [";

                for(int i =0; methodList != null && i<methodList.size();i++){
                    if(i != 0){
                        abstractClassComponent += ",";
                    }
                    abstractClassComponent += "'"+methodList.get(i)+"'";
                }
                abstractClassComponent += "],"
                +"attrs: {"
                +"'.uml-class-name-rect': {"
                +"fill: '#68ddd5',"
                        +" stroke: '#ffffff',"
                        +"'stroke-width': 0.5"
                +"},"
                +"'.uml-class-attrs-rect': {"
                +"fill: '#9687fe',"
                        +"stroke: '#fff',"
                        +"'stroke-width': 0.5"
                +"},"
                +"'.uml-class-methods-rect': {"
                +"fill: '#9687fe',"
                        +"stroke: '#fff',"
                        +"'stroke-width': 0.5"
                +"},"
                +"'.uml-class-methods-text, .uml-class-attrs-text': {"
                +"fill: '#fff'"
                +"}}})";
        return abstractClassComponent;
    }
    private String createConcreteClass(final String objName, List<String> attributeList, final List<String> methodList, int[] size, int[] position){

        String concreteClass = objName+": new uml.Class({"
                +"position: { x:"+position[0]+"  , y: "+position[1]+" },"
                +"size: { width: "+size[0]+", height: "+size[1]+" },"
                +"name: '"+objName+"',"
                +"attributes: [" ;
                for(int i =0; attributeList != null && i<attributeList.size();i++){
                    if(i != 0){
                        concreteClass += ",";
                    }
                    concreteClass += "'"+attributeList.get(i)+"'";
                }
                concreteClass += "],"
                +"methods: [";

                for(int i =0; methodList != null && i<methodList.size();i++){
                    if(i != 0){
                        concreteClass += ",";
                    }
                    concreteClass += "'"+methodList.get(i)+"'";
                }
                concreteClass += "],"
                +"attrs: {"
                +"'.uml-class-name-rect': {"
                +"fill: '#ff8450',"
                        +"stroke: '#fff',"
                        +"'stroke-width': 0.5"
                +"},"
                +"'.uml-class-attrs-rect': {"
                +"fill: '#fe976a',"
                        +"stroke: '#fff',"
                        +"'stroke-width': 0.5"
                +"},"
                +"'.uml-class-methods-rect': {"
                +"fill: '#fe976a',"
                        +"stroke: '#fff',"
                        +"'stroke-width': 0.5"
                +"},"
                +"'.uml-class-attrs-text': {"
                +"'ref-y': 0.5,"
                        +"'y-alignment': 'middle'"
                +"} } })";
        return concreteClass;
    }
    private String createLinks(Collaboration collaboration, String sourceName, String targetName){
        String links = "" ;
            if(Collaboration.GENERALIZATION.equals(collaboration)){
                links += "new uml.Generalization({ source: { id: classes."+sourceName+".id }, target: { id: classes."+targetName+".id }})";
            }else if(Collaboration.AGGREGATION.equals(collaboration)){
                links += "new uml.Aggregation({ source: { id: classes."+sourceName+".id }, target: { id: classes."+targetName+".id }})";
            }else if(Collaboration.COMPOSITE.equals(collaboration)){
                links += "new uml.Composition({ source: { id: classes."+sourceName+".id }, target: { id: classes."+targetName+".id }})";
            }else if(Collaboration.IMPLEMENTATION.equals(collaboration)){
                links += "new uml.Implementation({ source: { id: classes."+sourceName+".id }, target: { id: classes."+targetName+".id }})";
            }else if(Collaboration.ASSOCIATION.equals(collaboration)){
                links += "new uml.Association({ source: { id: classes."+sourceName+".id }, target: { id: classes."+targetName+".id }})";
            }


        return links;
    }
    private int [] calculateGraphSize(){
        return new int[]{10000,10000};
    }
    private int [] calculateComponentSize(){
        return new int[]{200,100};
    }
    private int [] calculatePosition(){return new int[]{positionX,positionY};}
    private void updatePosition(){
        if(objCount%2!=0){
            positionX += 300;
        }else{
            positionY += 200;
            positionX = 10 ;
        }

    }
    private void setValuesToDefault(){
        objCount=0;
        positionX = 10;
        positionY = 10;
    }
    public List<String> generatedURLClassDiagram(UMLClassDiagramObjectStructure umlClassDiagramObjectStructure){
        int graphSize[] = calculateGraphSize();
        List<String> umlDiagramComponentList = new ArrayList<>();

        String umlClassDiagram = initializePanel("uml-class-diagram",graphSize[0],graphSize[1]);
        umlDiagramComponentList.add(umlClassDiagram);
        umlClassDiagram = "var classes = {" ;
        int count1 =0;
        for(UMLClassDiagramObject umlClassDiagramObject :umlClassDiagramObjectStructure.getUmlClassDiagramObjectSet()){
             String classCode = "";
            if(Representaion.ABSTARCTCLASS.equals(umlClassDiagramObject.getRepresentaion())){

                classCode = createAbstractClass(umlClassDiagramObject.getObjName(),umlClassDiagramObject.getAttributeList(),umlClassDiagramObject.getMethodList(),calculateComponentSize(),calculatePosition());
                updatePosition();
            }else if (Representaion.CONCREATECLASS.equals(umlClassDiagramObject.getRepresentaion())){

                classCode = createConcreteClass(umlClassDiagramObject.getObjName(),umlClassDiagramObject.getAttributeList(),umlClassDiagramObject.getMethodList(),calculateComponentSize(),calculatePosition());
                updatePosition();
            }else if (Representaion.INTERFACE.equals(umlClassDiagramObject.getRepresentaion())){

                classCode = createInterface(umlClassDiagramObject.getObjName(),umlClassDiagramObject.getMethodList(),umlClassDiagramObject.getAttributeList(),calculateComponentSize(),calculatePosition());
                updatePosition();
            }
            if(classCode != "" && count1!= 0){
                umlClassDiagram+= "," +classCode;
                count1 ++;
                objCount++;
            }
            if(classCode != "" && count1== 0){
                umlClassDiagram+= classCode;
                count1 ++;
                objCount++;
            }

        }
        umlClassDiagram += "}";
        umlDiagramComponentList.add(umlClassDiagram);
        umlClassDiagram =null;
        umlClassDiagram = "Object.keys(classes).forEach(function(key) {"
                +"graph2.addCell(classes[key]);"
                +"});";

        umlDiagramComponentList.add(umlClassDiagram);
        umlClassDiagram ="var relations = [";
        int count =0;
        for(UMLClassDiagramObject umlClassDiagramObject :umlClassDiagramObjectStructure.getUmlClassDiagramObjectSet()) {

            for (String connectedObjName : umlClassDiagramObject.getConnectedObjName()){
                String linksCode = createLinks(umlClassDiagramObject.getCollaboration(), umlClassDiagramObject.getObjName(), connectedObjName);
            if (linksCode != "" && count != 0) {
                umlClassDiagram += "," + linksCode;
                count++;
            }
            if (linksCode != "" && count == 0) {
                umlClassDiagram += linksCode;
                count++;
            }
        }
        }
        umlClassDiagram += " ];";
        umlDiagramComponentList.add(umlClassDiagram);
        umlClassDiagram ="Object.keys(relations).forEach(function(key) {" +
                "        graph2.addCell(relations[key]);" +
                "        });";
        umlDiagramComponentList.add(umlClassDiagram);
        return umlDiagramComponentList;
    }
}
