package uom.cse.msc.designPatternSelector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.Participant;
import uom.cse.msc.designPatternSelector.repositories.ParticipantRepository;

import java.util.List;

@Service
public class ParticipantService {

    @Autowired
    private ParticipantRepository participantRepository;

    public List<Participant> saveAll(List<Participant> participant){
        return  participantRepository.saveAll(participant);
    }

    public Participant save(Participant participant){
        return  participantRepository.save(participant);
    }

    public Participant findById(int id){
        return  participantRepository.findById(id);
    }
}
