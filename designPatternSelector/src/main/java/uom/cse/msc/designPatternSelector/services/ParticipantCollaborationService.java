package uom.cse.msc.designPatternSelector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.ParticipantCollaboration;
import uom.cse.msc.designPatternSelector.repositories.ParticipantCollaborationRepository;

import java.util.List;

@Service
public class ParticipantCollaborationService {

    @Autowired
    private ParticipantCollaborationRepository participantCollaborationRepository;

    public List<ParticipantCollaboration> saveAll(final List<ParticipantCollaboration>participantCollaborationList){
        return participantCollaborationRepository.saveAll(participantCollaborationList);
    }

    public ParticipantCollaboration save(final ParticipantCollaboration participantCollaboration){
        return participantCollaborationRepository.save(participantCollaboration);
    }
}
