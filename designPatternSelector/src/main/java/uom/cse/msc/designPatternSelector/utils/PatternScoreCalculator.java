package uom.cse.msc.designPatternSelector.utils;

import org.springframework.stereotype.Component;
import uom.cse.msc.designPatternSelector.models.PatternCritRank;
import uom.cse.msc.designPatternSelector.models.PatternRelationship;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class PatternScoreCalculator {

    public List<Integer> calculatePatternCritScores(final List<PatternCritRank> patternCritRankList) {
        Map<Integer, Integer> patternScore = new HashMap<>();
        for (PatternCritRank patternCritRank : patternCritRankList) {
            if (patternScore.containsKey(patternCritRank.getPatternId())) {
                patternScore.put(patternCritRank.getPatternId(),patternScore.get(patternCritRank.getPatternId()) + patternCritRank.getRank());
            } else {
                patternScore.put(patternCritRank.getPatternId(), patternCritRank.getRank());
            }

        }
        Integer max = Collections.max(patternScore.entrySet(), Map.Entry.comparingByValue()).getValue();
        List<Integer> listOfMax = patternScore.entrySet()
                .stream()
                .filter(entry -> entry.getValue() == max)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        return listOfMax;
    }

    public Set<Integer> getPatternByRelationShip(final List<Integer> patternSuggestionsPrevious,
                                                          final List<Integer> patternSuggestionCurrent,
                                                          final Map<Integer,List<Integer>> patternRelationships) {

        Set<Integer> suggestionSet = new HashSet<>();
        for (Integer patternIdPre :patternSuggestionsPrevious) {
            if(patternRelationships.containsKey(patternIdPre)){
                for (Integer patternId: patternRelationships.get(patternIdPre)) {
                    if(patternSuggestionCurrent.contains(patternId)){
                       suggestionSet.add(patternId);
                    }
                }

            }
        }
        return suggestionSet;
    }
}
