package uom.cse.msc.designPatternSelector.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uom.cse.msc.designPatternSelector.models.PatternRelationship;
import uom.cse.msc.designPatternSelector.services.PatternRelationshipService;

import java.util.List;

@RestController
@RequestMapping("/patternRelationship")
public class PatternRelationshipController {
    @Autowired
    private PatternRelationshipService patternRelationshipService;

    public List<PatternRelationship> create(@RequestBody List<PatternRelationship> patternRelationshipList){
        return patternRelationshipService.saveAll(patternRelationshipList);
    }
}
