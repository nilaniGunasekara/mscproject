package uom.cse.msc.designPatternSelector.models;

public enum PatternCategory {
    CREATIONAL,
    STRUCTRAL,
    BEHAVIOURAL;

}
