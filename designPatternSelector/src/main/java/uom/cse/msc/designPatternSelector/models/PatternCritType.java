package uom.cse.msc.designPatternSelector.models;

public enum PatternCritType {
    GENERAL,
    Object_creation,
    Object_count,
    System_count,
    Object_representations,
    Object_structure,
    Object_creation_cost,
    Implementation,
    Object_state,
    Collections,
    Algorithms,
    Consequences,
    Work
}
