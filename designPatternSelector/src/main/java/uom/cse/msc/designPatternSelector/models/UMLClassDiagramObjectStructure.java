package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class UMLClassDiagramObjectStructure {
    /**
     *
     */
    private Set<UMLClassDiagramObject> umlClassDiagramObjectSet;
}
