package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import java.util.List;

@Data
public class ComponentDetails {
    private String name;
    private int id;
    private List<SelectedPatternCrit> selectedPatternCritList;
    private List<ComponentInteraction> componentInteractionList;
}
