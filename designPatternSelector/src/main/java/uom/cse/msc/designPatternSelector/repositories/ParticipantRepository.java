package uom.cse.msc.designPatternSelector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uom.cse.msc.designPatternSelector.models.Participant;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Integer> {
    Participant findById(int id);
}
