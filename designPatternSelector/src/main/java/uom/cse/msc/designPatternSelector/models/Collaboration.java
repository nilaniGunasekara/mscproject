package uom.cse.msc.designPatternSelector.models;

public enum Collaboration {
    GENERALIZATION,
    IMPLEMENTATION,
    DEPENDANCY,
    COMPOSITE,
    AGGREGATION,
    ASSOCIATION;
}
