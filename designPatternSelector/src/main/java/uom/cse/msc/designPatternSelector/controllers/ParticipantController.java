package uom.cse.msc.designPatternSelector.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uom.cse.msc.designPatternSelector.models.Participant;
import uom.cse.msc.designPatternSelector.services.ParticipantService;

import java.util.List;

@RestController
@RequestMapping("/participant")
public class ParticipantController {

    @Autowired
    private ParticipantService participantService;

    @RequestMapping(value = "/add" , method = RequestMethod.POST)
    public List<Participant> add(@RequestBody List<Participant> participantList){
        return participantService.saveAll(participantList);
    }
}
