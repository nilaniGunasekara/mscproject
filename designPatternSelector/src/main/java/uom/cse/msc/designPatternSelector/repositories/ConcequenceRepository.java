package uom.cse.msc.designPatternSelector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uom.cse.msc.designPatternSelector.models.Consequences;

@Repository
public interface ConcequenceRepository extends JpaRepository<Consequences,Integer> {
}
