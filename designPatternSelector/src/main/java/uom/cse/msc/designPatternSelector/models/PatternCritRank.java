package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "patternCrit_rank")
public class PatternCritRank {
    @Id
    @GeneratedValue
    private  int id;
    private int patternId;
    private int pattrnCritId;
    private int rank;
}
