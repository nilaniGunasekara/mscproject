package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.validation.constraints.Null;
import java.util.List;

@Data
public class SelectedPatternCrit {
    private PatternCategory patternCategory;
    private List<Integer> patternCritId;
}
