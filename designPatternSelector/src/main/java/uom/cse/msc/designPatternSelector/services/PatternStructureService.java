package uom.cse.msc.designPatternSelector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.PatternStructure;
import uom.cse.msc.designPatternSelector.repositories.PatternStructureRepository;

@Service
public class PatternStructureService {

    @Autowired
    private PatternStructureRepository patternStructureRepository;

    public PatternStructure create(PatternStructure patternStructure){
        return patternStructureRepository.save(patternStructure);
    }


}
