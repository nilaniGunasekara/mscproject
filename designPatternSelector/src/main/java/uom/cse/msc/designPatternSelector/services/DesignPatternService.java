package uom.cse.msc.designPatternSelector.services;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.*;
import uom.cse.msc.designPatternSelector.repositories.DesignPatternRepository;
import uom.cse.msc.designPatternSelector.utils.PatternScoreCalculator;
import uom.cse.msc.designPatternSelector.utils.UMLClassGenerator;

import java.util.*;

@Service
public class DesignPatternService {

    @Autowired
    private DesignPatternRepository designPatternRepository;

    @Autowired
    private PatternStructureService patternStructureService;

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private ConsequencesService consequencesService;

    @Autowired
    private RelatedPatternService relatedPatternService;

    @Autowired
    private PatternCriteriaService patternCriteriaService;

    @Autowired
    private PatternRelationshipService patternRelationshipService;

    @Autowired
    private PatternCritRankService patternCritRankService;

    @Autowired
    private ParticipantCollaborationService participantCollaborationService;

    @Autowired
    private ApplicabilityService applicabilityService;

    @Autowired
    private PatternScoreCalculator patternScoreCalculator;

    @Autowired
    private UMLClassGenerator umlClassGenerator;



    private static Map<Integer, List<Integer>> relatedPatternMap = new HashMap<>();

    public DesignPattern create(DesignPattern designPattern){
     /*   designPattern.getPatternStructure().getParticipantCollaborationsList().stream().forEach(participantCollaboration -> {
            participantCollaboration.setSourceParticipant(participantService.save(participantCollaboration.getSourceParticipant()));
            participantCollaboration.setTargetParticipant(participantService.save(participantCollaboration.getTargetParticipant()));
        });*/
        //designPattern.getPatternStructure().setParticipantList(participantService.saveAll(designPattern.getPatternStructure().getParticipantList()));
        designPattern.setApplicabilities(applicabilityService.saveAll(designPattern.getApplicabilities()));
        designPattern.getPatternStructure().setParticipantCollaborationsList(participantCollaborationService.saveAll(designPattern.getPatternStructure().getParticipantCollaborationsList()));

        designPattern.setPatternCriteria(patternCriteriaService.saveAll(designPattern.getPatternCriteria()));
        designPattern.setRelatedPatternList(relatedPatternService.saveAll(designPattern.getRelatedPatternList()));
        designPattern.setConsequencesList(consequencesService.saveAll(designPattern.getConsequencesList()));
        designPattern.setPatternStructure( patternStructureService.create(designPattern.getPatternStructure()));
        return designPatternRepository.saveAndFlush(designPattern);
    }

    public DesignPatternRecomendationResponse getRecommendations(final DesignPatternRecomendationRequest recomendationRequest){
       // patternRelationshipService.getRelatedPatterns().stream().collect(Collectors.toMap(PatternRelationship::getId,PatternRelationship::getRelatedPatternId));

        if(relatedPatternMap.isEmpty()){
            createRelatedPatternMap();
        }
        DesignPattern designPattern=null;

        List<DesignPattern> suggestedPatternByWA = getSuggestedPatternByWA(recomendationRequest);
        Map<Integer,List<DesignPattern>> suggestedPattern = new HashMap<>();
        Map<Integer,List<Integer>> suggestedPatternMap = getSuggestedPatternsForComponents(recomendationRequest);
        for(Integer key : suggestedPatternMap.keySet()){
            List<DesignPattern> suggestedPattern1 =designPatternRepository.findByIdIn(suggestedPatternMap.get(key));
            suggestedPattern1.addAll(suggestedPatternByWA);
            suggestedPattern.put(key ,suggestedPattern1);
            designPattern=suggestedPattern.get(key).get(0);
        }
        if(!suggestedPatternByWA.isEmpty()){
            for(DesignPattern designPattern1 :suggestedPatternByWA){
                if(!"Singleton".equalsIgnoreCase(designPattern1.getName())){
                    designPattern = designPattern1;
                }
            }

        }
        if(suggestedPattern != null && !suggestedPattern.isEmpty()){
                UMLClassDiagramObjectStructure umlClassDiagramObjectStructure =mapComponentsToStructureParticipant(suggestedPattern,recomendationRequest);
                if(umlClassDiagramObjectStructure.getUmlClassDiagramObjectSet() != null && !umlClassDiagramObjectStructure.getUmlClassDiagramObjectSet().isEmpty()){
                List<String> UmlClassDiagram = umlClassGenerator.generatedURLClassDiagram(umlClassDiagramObjectStructure);
                DesignPatternRecomendationResponse recomendationResponse = new DesignPatternRecomendationResponse();
                //recomendationResponse.setDesignPatternList(suggestedPattern);
                recomendationResponse.setUmlDiagramList(UmlClassDiagram);
                recomendationResponse.setApplicabilityList(designPattern.getApplicabilities());
                recomendationResponse.setConsequencesList(designPattern.getConsequencesList());
                recomendationResponse.setPatternCategory(designPattern.getPatternCategory());
                recomendationResponse.setPatternIntent(designPattern.getIntent());
                recomendationResponse.setPatternName(designPattern.getName());
                recomendationResponse.setPatternExplanation(designPattern.getDescriptionList());
                recomendationResponse.setStatus("SUCCESS");
                return recomendationResponse;
            }else {
                    DesignPatternRecomendationResponse recomendationResponse = new DesignPatternRecomendationResponse();
                    //recomendationResponse.setDesignPatternList(suggestedPattern);
                    recomendationResponse.setErrorMessage("Unable to suggest a pattern for the given scenario. Please try it in a different way.");
                    recomendationResponse.setStatus("NOTFOUND");
                    return recomendationResponse;
                }

        }else{

            DesignPatternRecomendationResponse recomendationResponse = new DesignPatternRecomendationResponse();
            //recomendationResponse.setDesignPatternList(suggestedPattern);
            recomendationResponse.setErrorMessage("Unable to suggest a pattern for the given scenario. Please try it in a different way.");
            recomendationResponse.setStatus("NOTFOUND");
            return recomendationResponse;
        }

    }

    private List<DesignPattern> getSuggestedPatternByWA(DesignPatternRecomendationRequest recomendationRequest) {
        List<DesignPattern> designPatternList = new ArrayList<>();
        for(IntentObject intentObject :recomendationRequest.getSelectedIntentList()){
            if(intentObject.getConfidence()>7.5){
                designPatternList.add(designPatternRepository.findByName(intentObject.getIntent()));
            }
        }
        return designPatternList;
    }

    private UMLClassDiagramObjectStructure mapComponentsToStructureParticipant(Map<Integer, List<DesignPattern>> suggestedPattern,
                                                                               DesignPatternRecomendationRequest recomendationRequest) {

        UMLClassDiagramObjectStructure umlClassDiagramObjectStructure = new UMLClassDiagramObjectStructure();
        Set<UMLClassDiagramObject> umlClassDiagramObjectList = new HashSet<>();
        Map<Integer,Map<Integer,String>> structCompnentMap = new HashMap<>();
        Map<Integer,String> structCompnentDetailsMap = new HashMap<>();
        Map<String,List<String> >namesMap = new HashMap<>();
        Map<Integer,List<Integer>> idsMap = new HashMap<>();
        for(ComponentDetails componentDetails : recomendationRequest.getComponentDetailsList()){
            int componenetId = componentDetails.getId();
            List<DesignPattern> designPatternsList = suggestedPattern.get(componenetId);
            if(designPatternsList == null || designPatternsList.isEmpty()){
                continue;
            }
            for(DesignPattern designPattern: designPatternsList){

                PatternStructure patternStructure = designPattern.getPatternStructure();
                List<ParticipantCollaboration> participantCollaborationList = patternStructure.getParticipantCollaborationsList();
                for(ParticipantCollaboration participantCollaboration : participantCollaborationList){
                    //Participant participant = participantCollaboration.getParticipant();
                    UMLClassDiagramObject umlClassDiagramObject = new UMLClassDiagramObject();
                    structCompnentDetailsMap.put(participantCollaboration.getSourceParticipantId(),participantCollaboration.getSourceParticipantName());
                    if(participantCollaboration.getIsNewMember().equals("true")){
                        umlClassDiagramObject.setObjId(participantCollaboration.getSourceParticipantId());
                        umlClassDiagramObject.setObjName(participantCollaboration.getSourceParticipantName());
                        umlClassDiagramObject.setRepresentaion(participantCollaboration.getRepresentaion());
                        umlClassDiagramObject.setCollaboration(participantCollaboration.getCollaboration());
                        Participant participant = participantService.findById(participantCollaboration.getSourceParticipantId());
                        umlClassDiagramObject.setMethodList(participant.getMethodList());
                        umlClassDiagramObject.setAttributeList(participant.getAttributeList());

                    }else{
                        List<ComponentInteraction> componentInteractionList = componentDetails.getComponentInteractionList();
                        for (ComponentInteraction componentInteraction1 : componentInteractionList ){
                            if((ComponentRelationType.MUSTHAVE.equals(componentInteraction1.getComponentRelationType())&&
                                    Collaboration.COMPOSITE.equals(participantCollaboration.getCollaboration()))
                            ||(ComponentRelationType.HAS_A.equals(componentInteraction1.getComponentRelationType())&&
                                    Collaboration.AGGREGATION.equals(participantCollaboration.getCollaboration()))
                            || (ComponentRelationType.IS_A.equals(componentInteraction1.getComponentRelationType())&&
                                    Collaboration.GENERALIZATION.equals(participantCollaboration.getCollaboration()))
                            || (ComponentRelationType.USE.equals(componentInteraction1.getComponentRelationType())&&
                                    Collaboration.ASSOCIATION.equals(participantCollaboration.getCollaboration()))
                            || (ComponentRelationType.IS_A.equals(componentInteraction1.getComponentRelationType())&&
                                    Collaboration.IMPLEMENTATION.equals(participantCollaboration.getCollaboration()))){
                                umlClassDiagramObject.setObjId(componenetId);
                                umlClassDiagramObject.setObjName(componentDetails.getName());
                                umlClassDiagramObject.setRepresentaion(participantCollaboration.getRepresentaion());
                                umlClassDiagramObject.setCollaboration(participantCollaboration.getCollaboration());
                                Participant participant = participantService.findById(participantCollaboration.getSourceParticipantId());
                                umlClassDiagramObject.setMethodList(participant.getMethodList());
                                umlClassDiagramObject.setAttributeList(participant.getAttributeList());

                                if(!namesMap.containsKey(participantCollaboration.getSourceParticipantName())) {
                                    List<String> compNameList = new ArrayList<>();
                                    compNameList.add(componentDetails.getName());
                                    namesMap.put(participantCollaboration.getSourceParticipantName(),compNameList);

                                }else{
                                    List<String> compNameList = namesMap.get(participantCollaboration.getSourceParticipantName());
                                    compNameList.add(componentDetails.getName());
                                    namesMap.put(participantCollaboration.getSourceParticipantName(),compNameList);
                                }

                                if(!idsMap.containsKey(participantCollaboration.getSourceParticipantId())) {
                                    List<Integer> compIdList = new ArrayList<>();
                                    compIdList.add(componenetId);
                                    idsMap.put(participantCollaboration.getSourceParticipantId(),compIdList);

                                }else{
                                    List<Integer> compIdList = idsMap.get(participantCollaboration.getSourceParticipantId());
                                    compIdList.add(componenetId);
                                    idsMap.put(participantCollaboration.getSourceParticipantId(),compIdList);
                                }

                               /* if(!structCompnentMap.containsKey(participantCollaboration.getSourceParticipantId())) {
                                    Map<Integer, String> copmMap = new HashMap<>();
                                    copmMap.put(componenetId, componentDetails.getName());
                                    structCompnentMap.put(participantCollaboration.getSourceParticipantId(), copmMap);
                                }else{
                                    Map<Integer, String> copmMap = structCompnentMap.get(participantCollaboration.getSourceParticipantId());
                                    copmMap.put(componenetId, componentDetails.getName());
                                    structCompnentMap.put(participantCollaboration.getSourceParticipantId(), copmMap);
                                }*/

                            }
                        }
                        if(componentInteractionList.isEmpty() && participantCollaboration.getTargetParticipantName().isEmpty()){
                            //umlClassDiagramObject.setObjName(participantCollaboration.getSourceParticipantName());
                            umlClassDiagramObject.setObjId(componentDetails.getId());
                            umlClassDiagramObject.setRepresentaion(participantCollaboration.getRepresentaion());
                            umlClassDiagramObject.setObjName(componentDetails.getName());
                            umlClassDiagramObject.setCollaboration(participantCollaboration.getCollaboration());
                            Participant participant = participantService.findById(participantCollaboration.getSourceParticipantId());
                            umlClassDiagramObject.setMethodList(participant.getMethodList());
                            umlClassDiagramObject.setAttributeList(participant.getAttributeList());

                            if(!namesMap.containsKey(participantCollaboration.getSourceParticipantName())) {
                                List<String> compNameList = new ArrayList<>();
                                compNameList.add(componentDetails.getName());
                                namesMap.put(participantCollaboration.getSourceParticipantName(),compNameList);

                            }else{
                                List<String> compNameList = namesMap.get(participantCollaboration.getSourceParticipantName());
                                compNameList.add(componentDetails.getName());
                                namesMap.put(participantCollaboration.getSourceParticipantName(),compNameList);
                            }

                            if(!idsMap.containsKey(participantCollaboration.getSourceParticipantId())) {
                                List<Integer> compIdList = new ArrayList<>();
                                compIdList.add(componenetId);
                                idsMap.put(participantCollaboration.getSourceParticipantId(),compIdList);

                            }else{
                                List<Integer> compIdList = idsMap.get(participantCollaboration.getSourceParticipantId());
                                compIdList.add(componenetId);
                                idsMap.put(participantCollaboration.getSourceParticipantId(),compIdList);
                            }
                            /*if(!structCompnentMap.containsKey(participantCollaboration.getSourceParticipantId())) {
                                Map<Integer, String> copmMap = new HashMap<>();
                                copmMap.put(componenetId, componentDetails.getName());
                                structCompnentMap.put(participantCollaboration.getSourceParticipantId(), copmMap);
                            }else{
                                Map<Integer, String> copmMap = structCompnentMap.get(participantCollaboration.getSourceParticipantId());
                                copmMap.put(componenetId, componentDetails.getName());
                                structCompnentMap.put(participantCollaboration.getSourceParticipantId(), copmMap);
                            }*/
                        }
                        /*if(componentInteractionList.isEmpty() && !participantCollaboration.getTargetParticipantName().isEmpty()){
                            //umlClassDiagramObject.setObjName(participantCollaboration.getSourceParticipantName());
                            umlClassDiagramObject.setObjId(componentDetails.getId());
                            umlClassDiagramObject.setRepresentaion(participantCollaboration.getRepresentaion());
                            umlClassDiagramObject.setCollaboration(participantCollaboration.getCollaboration());
                            umlClassDiagramObject.setObjName(componentDetails.getName());
                            if(!namesMap.containsKey(participantCollaboration.getSourceParticipantName())) {
                                List<String> compNameList = new ArrayList<>();
                                compNameList.add(componentDetails.getName());
                                namesMap.put(participantCollaboration.getSourceParticipantName(),compNameList);

                            }else{
                                List<String> compNameList = namesMap.get(participantCollaboration.getSourceParticipantName());
                                compNameList.add(componentDetails.getName());
                                namesMap.put(participantCollaboration.getSourceParticipantName(),compNameList);
                            }

                            if(!idsMap.containsKey(participantCollaboration.getSourceParticipantId())) {
                                List<Integer> compIdList = new ArrayList<>();
                                compIdList.add(componenetId);
                                idsMap.put(participantCollaboration.getSourceParticipantId(),compIdList);

                            }else{
                                List<Integer> compIdList = idsMap.get(participantCollaboration.getSourceParticipantId());
                                compIdList.add(componenetId);
                                idsMap.put(participantCollaboration.getSourceParticipantId(),compIdList);
                            }
                            *//*if(!structCompnentMap.containsKey(participantCollaboration.getSourceParticipantId())) {
                                Map<Integer, String> copmMap = new HashMap<>();
                                copmMap.put(componenetId, componentDetails.getName());
                                structCompnentMap.put(participantCollaboration.getSourceParticipantId(), copmMap);
                            }else{
                                Map<Integer, String> copmMap = structCompnentMap.get(participantCollaboration.getSourceParticipantId());
                                copmMap.put(componenetId, componentDetails.getName());
                                structCompnentMap.put(participantCollaboration.getSourceParticipantId(), copmMap);
                            }*//*
                        }*/


                    }
                    /*System.out.println("structCompnentMap 1"+ structCompnentMap);
                    System.out.println("structCompnentDetailsMap 1"+ structCompnentDetailsMap);
                    for(Integer structComId : structCompnentMap.keySet()){
                        if(participantCollaboration.getTargetParticipantId().contains(structComId)){
                            List<Integer> idList = participantCollaboration.getTargetParticipantId();
                            System.out.println("idList 1"+ idList);
                            idList.addAll(structCompnentMap.get(structComId).keySet());
                            System.out.println("idList 2"+ idList);
                            idList.remove(structComId);
                            System.out.println("idList 3"+ idList);
                            participantCollaboration.setTargetParticipantId(idList);
                            System.out.println("idList 4"+ idList);

                            List<String> nameList = participantCollaboration.getTargetParticipantName();
                            System.out.println("nameList 1"+ nameList);
                            nameList.addAll(structCompnentMap.get(structComId).values());
                            System.out.println("nameList 2"+ nameList);
                            nameList.remove(structCompnentDetailsMap.get(structComId));
                            System.out.println("nameList 3"+ nameList);
                            participantCollaboration.setTargetParticipantName(nameList);
                            System.out.println("nameList 4"+ nameList);
                        }


                    }*/

                        umlClassDiagramObject.setConnectedObjid(participantCollaboration.getTargetParticipantId());
                        umlClassDiagramObject.setConnectedObjName(participantCollaboration.getTargetParticipantName());




                    umlClassDiagramObjectList.add(umlClassDiagramObject);
                }

            }

        }
       // prprocessUmlObjects(umlClassDiagramObjectList,namesMap,idsMap);
        if(umlClassDiagramObjectList != null) {
            umlClassDiagramObjectStructure.setUmlClassDiagramObjectSet(preprocessUmlObjects(umlClassDiagramObjectList, namesMap, idsMap));
        }
        return umlClassDiagramObjectStructure;
    }

    private Set<UMLClassDiagramObject> preprocessUmlObjects(Set<UMLClassDiagramObject> umlClassDiagramObjectList, Map<String, List<String>> namesMap, Map<Integer, List<Integer>> idsMap) {

        System.out.println("namesMap "+ namesMap);
        System.out.println("idsMap "+ idsMap);

        for(UMLClassDiagramObject umlClassDiagramObject : umlClassDiagramObjectList){

            for(String name : namesMap.keySet()){
                System.out.println("namesMap "+ name);
             if(umlClassDiagramObject.getConnectedObjName().contains(name) ){
                 List<String> conectedObjNames = umlClassDiagramObject.getConnectedObjName();
                 System.out.println("conectedObjNames 1"+ conectedObjNames);
                 conectedObjNames.remove(name);
                 if(conectedObjNames.contains(umlClassDiagramObject.getObjName()) ){
                     conectedObjNames.remove(umlClassDiagramObject.getObjName());
                 }
                 System.out.println("namesMap.get(name) 1"+ namesMap.get(name));
                 conectedObjNames.addAll(namesMap.get(name));
                 System.out.println("conectedObjNames 2"+ conectedObjNames);
                 umlClassDiagramObject.setConnectedObjName(conectedObjNames);
             }
            }
            for(Integer id : idsMap.keySet()){
                System.out.println("idsMap "+ id);
                if(umlClassDiagramObject.getConnectedObjid().contains(id) ){
                    List<Integer> conectedObjIds = umlClassDiagramObject.getConnectedObjid();
                    System.out.println("conectedObjIds 1"+ conectedObjIds);
                    conectedObjIds.remove(id);
                    if(conectedObjIds.contains(umlClassDiagramObject.getObjId())){
                        conectedObjIds.remove(umlClassDiagramObject.getObjId());
                    }
                    System.out.println("1idsMap.get(id)"+ idsMap.get(id));
                    conectedObjIds.addAll(idsMap.get(id));
                    System.out.println("conectedObjIds 2"+ conectedObjIds);
                    umlClassDiagramObject.setConnectedObjid(conectedObjIds);
                }
            }
        }
        return umlClassDiagramObjectList;
    }


    public void createRelatedPatternMap(){
       // Map<Integer, List<Integer>> relatedPatternMap = new HashMap<>();
        for(PatternRelationship patternRelationship : patternRelationshipService.getRelatedPatterns()){
            int patternId = patternRelationship.getPatternId();
            if(relatedPatternMap.containsKey(patternId)){
                List<Integer> relatedIds = relatedPatternMap.get(patternId);
                relatedIds.add(patternRelationship.getRelatedPatternId());
                relatedPatternMap.put(patternId,relatedIds);
            }else{
                List<Integer> relatedIds = new ArrayList<>();
                relatedIds.add(patternRelationship.getRelatedPatternId());
                relatedPatternMap.put(patternId,relatedIds);
            }
        }
    }

    public Map<Integer,List<Integer>> getSuggestedPatternsForComponents(final DesignPatternRecomendationRequest recomendationRequest){
        Map<Integer,List<Integer>> objectPatternSuggestionMap = new HashMap<>();
        for(ComponentDetails componentDetails : recomendationRequest.getComponentDetailsList()){
            int componenetId = componentDetails.getId();
            for(SelectedPatternCrit selectedPatternCrit : componentDetails.getSelectedPatternCritList()){
                List<PatternCritRank> list = patternCritRankService.getPatternCritRankById(selectedPatternCrit.getPatternCritId());
                List<PatternCritRank> patternCritRankList = patternCritRankService.getPatternCritRankById(selectedPatternCrit.getPatternCritId());

                if(patternCritRankList != null && !patternCritRankList.isEmpty()) {
                    List<Integer> suggestedPatternIdsCurr = patternScoreCalculator.calculatePatternCritScores(patternCritRankList);

                    if (objectPatternSuggestionMap.containsKey(componenetId)) {
                        List<Integer> suggestedPatternIdsPre = objectPatternSuggestionMap.get(componenetId);
                        suggestedPatternIdsPre.addAll(
                                patternScoreCalculator.getPatternByRelationShip(suggestedPatternIdsPre,
                                        suggestedPatternIdsCurr, relatedPatternMap)
                        );
                        objectPatternSuggestionMap.put(componenetId, suggestedPatternIdsPre);
                    } else {
                        List<Integer> suggestedPatternIdsPre = new ArrayList<>();
                        suggestedPatternIdsPre.addAll(suggestedPatternIdsCurr);
                        objectPatternSuggestionMap.put(componenetId, suggestedPatternIdsPre);
                    }
                }

            }

        }
        return  objectPatternSuggestionMap;
    }

}
