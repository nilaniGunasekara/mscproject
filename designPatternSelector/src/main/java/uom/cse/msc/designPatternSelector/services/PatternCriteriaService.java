package uom.cse.msc.designPatternSelector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.PatternCriteria;
import uom.cse.msc.designPatternSelector.repositories.PatternCriteriaRepository;

import java.util.List;

@Service
public class PatternCriteriaService {

    @Autowired
    private PatternCriteriaRepository patternCriteriaRepository;

    public PatternCriteria create(PatternCriteria patternCriteria){
        return patternCriteriaRepository.save(patternCriteria);
    }

    public List<PatternCriteria> saveAll(List<PatternCriteria> patternCriteria){
        return patternCriteriaRepository.saveAll(patternCriteria);
    }

    public List<PatternCriteria> findAll(){
        return patternCriteriaRepository.findAll();
    }


}
