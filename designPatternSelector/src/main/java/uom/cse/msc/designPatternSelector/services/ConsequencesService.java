package uom.cse.msc.designPatternSelector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.Consequences;
import uom.cse.msc.designPatternSelector.repositories.ConcequenceRepository;

import java.util.List;

@Service
public class ConsequencesService {

    @Autowired
    private ConcequenceRepository concequenceRepository;

    public Consequences create(Consequences consequences){
        return concequenceRepository.save(consequences);
    }

    public List<Consequences> saveAll(List<Consequences> consequences){
        return concequenceRepository.saveAll(consequences);
    }
}
