package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "particiapnt_collaboration")
public class ParticipantCollaboration {

    @Id
    @GeneratedValue
    private int id;
    @ElementCollection
    private List<Integer> targetParticipantId;
    @ElementCollection
    private List<String> targetParticipantName;
    private String sourceParticipantName;
    private int sourceParticipantId;
    private Representaion representaion;
    private String isNewMember;

    private Collaboration collaboration;

    @OneToOne
    private PatternStructure patternStructure;

}

