package uom.cse.msc.designPatternSelector.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uom.cse.msc.designPatternSelector.models.PatternCritRank;
import uom.cse.msc.designPatternSelector.services.PatternCritRankService;

import java.util.List;

@RestController
@RequestMapping("/critRank")
public class PatternCritRankController {

    @Autowired
    private PatternCritRankService patternCritRankService;

    @RequestMapping(value = "/add" , method = RequestMethod.POST)
    public List<PatternCritRank> create(@RequestBody List<PatternCritRank> patternCritRankList){
        return patternCritRankService.saveAll(patternCritRankList);
    }
}
