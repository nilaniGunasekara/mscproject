package uom.cse.msc.designPatternSelector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.Applicability;
import uom.cse.msc.designPatternSelector.repositories.ApplicabilityRepository;

import java.util.List;

@Service
public class ApplicabilityService {

    @Autowired
    private ApplicabilityRepository applicabilityRepository;

    public List<Applicability> saveAll(final List<Applicability> applicabilityList){
        return applicabilityRepository.saveAll(applicabilityList);
    }

    public Applicability save(final Applicability applicability){
        return applicabilityRepository.save(applicability);
    }


}
