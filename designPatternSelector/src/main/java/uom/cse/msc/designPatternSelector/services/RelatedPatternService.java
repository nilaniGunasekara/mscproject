package uom.cse.msc.designPatternSelector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uom.cse.msc.designPatternSelector.models.RelatedPattern;
import uom.cse.msc.designPatternSelector.repositories.RelatedPatternRepository;

import java.util.List;

@Service
public class RelatedPatternService {

    @Autowired
    private RelatedPatternRepository relatedPatternRepository;

    public RelatedPattern create(RelatedPattern relatedPattern){
        return relatedPatternRepository.save(relatedPattern);
    }

    public List<RelatedPattern> saveAll(List<RelatedPattern> relatedPattern){
        return relatedPatternRepository.saveAll(relatedPattern);
    }

    public List<RelatedPattern> getRelatedPatterns() {
        return relatedPatternRepository.findAll();
    }
}
