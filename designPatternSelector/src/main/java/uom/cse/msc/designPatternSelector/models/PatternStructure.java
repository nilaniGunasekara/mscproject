package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "pattern_structure")
public class PatternStructure {

    @Id
    @GeneratedValue
    private int id;

    @OneToMany
    private List<ParticipantCollaboration> participantCollaborationsList;

   /* @ManyToMany
    @JoinTable(
            name = "structure_participant",
            joinColumns = { @JoinColumn(name = "structure_id") },
            inverseJoinColumns = { @JoinColumn(name = "participant_id") }
    )
    private List<Participant> participantList;
*/
  /*  @OneToOne(mappedBy = "patternStructure")
    private DesignPattern designPattern;*/


}
