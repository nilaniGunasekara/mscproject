package uom.cse.msc.designPatternSelector.repositories;

import lombok.Data;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uom.cse.msc.designPatternSelector.models.ParticipantCollaboration;

@Repository
public interface ParticipantCollaborationRepository extends JpaRepository<ParticipantCollaboration,Integer> {
}
