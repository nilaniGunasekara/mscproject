package uom.cse.msc.designPatternSelector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uom.cse.msc.designPatternSelector.models.PatternRelationship;

import java.util.Arrays;

@Repository
public interface PatternRelationshipRepository extends JpaRepository<PatternRelationship, Integer> {

}
