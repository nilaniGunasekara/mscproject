package uom.cse.msc.designPatternSelector.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "related_patterns")
public class RelatedPattern {

    @Id
    @GeneratedValue
    @Column(name = "rp_id")
    private int id;
    private String name;

}
