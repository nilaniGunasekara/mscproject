package uom.cse.msc.designPatternSelector.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uom.cse.msc.designPatternSelector.models.PatternCriteria;
import uom.cse.msc.designPatternSelector.services.PatternCriteriaService;

import java.util.List;

@RestController
@RequestMapping("/patternCrit")
public class PatternCritController {

    @Autowired
    private PatternCriteriaService patternCriteriaService;

    @RequestMapping(value = "/getAll" , method = RequestMethod.GET)
    public List<PatternCriteria> getAll(){
        return patternCriteriaService.findAll();
    }
}
