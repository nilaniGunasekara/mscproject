
var buttonCount =0;
function updateComponenetDropDown(elementList){
    var compIsAList = document.getElementById("component-dropdown-is-a");
    var compAggList = document.getElementById("component-dropdown-agg");
    var compComList = document.getElementById("component-dropdown-comp");
    compIsAList.innerHTML = "";
    compAggList.innerHTML = "";
    compComList.innerHTML = "";
    for(var i = 0; i < elementList.length; i++) {
        var opt = elementList[i];
        compIsAList.innerHTML += '<li><a href="#">'+opt+'</a></li>';
        compAggList.innerHTML += '<li><a href="#">'+opt+'</a></li>';
        compComList.innerHTML += '<li><a href="#">'+opt+'</a></li>';
    }
}

$("body").on('click', '#component-dropdown-is-a li a', function () {
    var selectedValue = $(this).text();
    var select = document.getElementById("selected-component-is-a");
    select.innerHTML += '<li id="is_'+selectedValue+'_'+buttonCount+'"><a href="#">'+selectedValue+'&nbsp;</a><button type="button" class="btn btn-danger btn-xs" onclick="removeElement(is_'+selectedValue+'_'+buttonCount+')" > x</button></li>';
	buttonCount++;
	});

$("body").on('click', '#component-dropdown-agg li a', function () {
    var selectedValue = $(this).text();
    var select = document.getElementById("selected-component-agg");
    select.innerHTML += '<li><a href="#">'+selectedValue+'</a></li>';
});

$("body").on('click', '#component-dropdown-comp li a', function () {
    var selectedValue = $(this).text();
    var select = document.getElementById("selected-component-comp");
    select.innerHTML += '<li><a href="#">'+selectedValue+'</a></li>';
});



function setPatternCriteriaList2(patternCritList){
    var criteriaCheckBoxes = "";
    var critList = document.getElementById("pattern-crit-list");
    critList.innerHTML = "";

    for(var i = 0; i < patternCritList.length; i++) {
            var critDescription = patternCritList[i].description;
            var critId = patternCritList[i].id;

            critList.innerHTML +='<div class="col-md-12">'
                                        +'<div class="checkbox ">'
                                            +'<label for="prependedcheckbox">'+critDescription+'</label>'
                                            +'<input type="checkbox" id="'+critId+'" name="'+critId+'" value="1">'
                                        +'</div>'
                                    +'</div>';

    }
}


function setPatternCriteriaList(patternCritList){
    var criteriaCheckBoxes = "";

    var critList = document.getElementById("pattern-crit-list");
    critList.innerHTML = "";

    for(var i = 0; i < patternCritList.length; i++) {
		var critType = patternCritList[i].critType;
		    var subCritList = document.getElementsByName(critType);
			if(subCritList == null || subCritList.length==0){
				critList.innerHTML += '<div class="col-md-11 well well-sm" name="'+critType+'" data-toggle="collapse" data-target="#'+critType+'">'
				+'<a href="#">'+critType+'</a>'
				+'</div>'
				+'<div id="'+critType+'" class="col-md-12 pull-right collapse"></div>';
			}
			var critDiv = document.getElementById(critType);
			
            var critDescription = patternCritList[i].description;
            var critId = patternCritList[i].id;

            critDiv.innerHTML +='<div class="col-md-12 ">'
                                        +'<div class="checkbox  ">'
                                            +'<label for="prependedcheckbox">'+critDescription+'</label>'
                                            +'<input type="checkbox" id="'+critId+'" name="'+critId+'" value="1">'
                                        +'</div>'
                                    +'</div>';

    }
}

function test(){
    alert("This is a test");
}

function enableDropDown(checkboxId){
    var dropdownId = "";
    if(checkboxId == 'relations-is-a'){
        dropdownId = '#button-dropdown-is-a';
    }else if(checkboxId == 'relations-agg'){
        dropdownId = '#button-dropdown-agg';
    }else if(checkboxId == 'relations-comp'){
        dropdownId = '#button-dropdown-comp';
    }else{
        return;
    }
    checkboxId='#'+checkboxId;
    $(checkboxId).click(function() {
       if ($(this).is(':checked')) {
            $(dropdownId).prop("disabled", false);
       } else {
            $(dropdownId).prop('disabled', true);
       }
    });
}

function deleteSelectedComponent(elemet){
	var elem = document.getElementById(elemet);
elem.parentNode.removeChild(elem);
}
function removeElement(id) {
       var element = document.getElementById(id.id);
       element.parentNode.removeChild(element);
  }
  
  $(function() {

  $("#newModalForm").validate({
    rules: {
      textinput: {
        required: true,
        minlength: 8
      },
      action: "required"
    },
    messages: {
      textinput: {
        required: "Please enter some data",
        minlength: "Your data must be at least 8 characters"
      },
      action: "Please provide some data"
    }
  });
});