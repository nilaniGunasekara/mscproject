function apiTesting(){
    var request = new XMLHttpRequest();
    request.open('GET', 'https://ghibliapi.herokuapp.com/films', true);
    request.onload = function () {

        var data = JSON.parse(this.response);
        if (request.status >= 200 && request.status < 400) {
            data.forEach(movie => {
                console.log(movie.title);
            });
        } else {
            console.log('error');
        }
    }
    request.send();
 }

 function getAllCrit(){
     var request = new XMLHttpRequest();
     request.open('GET', 'http://localhost:8080/patternCrit/getAll', true);
request.setRequestHeader ("Authorization", "Basic " + btoa("user" + ":" + "23f16fd6-f3e7-4c08-a152-f31749d7097b"));
     request.onload = function () {

         var data = JSON.parse(this.response);
         if (request.status >= 200 && request.status < 400) {
            setPatternCriteriaList(data);
         } else {
             console.log('error');
         }
     }
     request.send();
  }

  function submitData(){
  var http = new XMLHttpRequest();
  var url = 'http://localhost:8080/designPattern/suggest';
  var reqBody = JSON.stringify(createRequestJson());
  http.open('POST', url, true);
http.setRequestHeader ("Authorization", "Basic " + btoa("user" + ":" + "23f16fd6-f3e7-4c08-a152-f31749d7097b"));


  //Send the proper header information along with the request
  http.setRequestHeader('Content-type', 'application/json; charset="utf-8"');

  http.onload = function() {//Call a function when the state changes.
      if(http.readyState == 4 && http.status == 200) {
          alert(http.responseText);
      }
  }
  http.send(reqBody);

  }
